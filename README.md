# Estudo do curso Descomplicando Terraform do LinuxTIPS

Este repositório serve como anotação do progresso nas aulas do curso 

## Anotações separadas por days

- [Day 1](https://gitlab.com/giuliana-marquesi/descomplicando-terraform-chamaasmina/-/blob/master/day1.md)
- [Day 2](https://gitlab.com/giuliana-marquesi/descomplicando-terraform-chamaasmina/-/blob/master/day2.md)
- [Day 3](https://gitlab.com/giuliana-marquesi/descomplicando-terraform-chamaasmina/-/blob/master/day3.md)

-----

## Lista de comandos

`terraform init` : 
>	ele inicializa a comunicação com os providers, baixando o que é necessário

`terraform plan` :
>	gera um plano que é um arquivo binário

`terraform apply` :
>	pega o plano, cria um estado (que seria uma "foto" de como seria o estado fnal desejado.
>	Depois,compara com estados anteriores e aplica o plano (?) gerando os recurosos necessários.
>	Faz um diff de estados

`terraform destroy`
>	deleta tudo que está provisionado de acordo com o estado

`terraform console` :
>	é console onde é possivel testar e treinar as expressoes

`terraform state pull`:
> Obtem o estado atual do arquivo **tfstate** do backend. Por padrão o retorno é na console (stdout)

`terraform state push`
> Envia para o backend um novo arquivo, de preferência nova versão, do **tfstate**

`terraform state list`
> Lista tudo o que foi criado segundo o statefile

`terraform state mv`
> Age da mesma forma que o comando `mv` do Linux. Pode ser bastante útil quando se quer redefinir estrutura do projeto sem matar as instâncias criadas, como por exemplo, com a definição de novos módulos para estruturas que já estavam rodando.

`terraform state rm`
> Remove o recurso da gerência do Terraform, excluindo as informações do statefile. É como se o recurso nunca tivesse existido. Ele não é apagado fisicamente do provedor. (Esse comando pode ser útil para a construção de um backend por exemplo)

`terraform import {recurso} {identificador}`
> Importa recursos legados, que não foram criados pelo terraform para dentro do state e consequentemente do terraform.

`terraform refresh`:
> Atualiza o *tfstate* consultando o provedor se houveram mudanças nos recursos

`terraform workspace new`
> Cria um novo workspace

`terraform workspace list`
> Lista os workspaces existentes

`terraform workspace select`
> Muda o ambiente para o workspace selecionado

`terraform taint`
> Marca um recurso como *tainted*, ou seja, no próximo ciclo irá ser destruído e recriado

`terraform untaint`
> Desmarca um recurso como *tainted*

`terraform graph`
> Exporta o grafo com as relações entre recursos de seu projeto terraform

`terraform fmt`
> Formata seus arquivos Terraform para o padrão estabelecido

`terraform validate`
> Valida o código localmente para identificar se há alguma configuração errada