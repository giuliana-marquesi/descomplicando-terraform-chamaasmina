# day 2

## Módulos

Serve para reunir logicamente os recursos e outros itens num mesmo lugar para facilitar a manipulação e organização dos projetos/produtos.

### Módulo Raíz

Módulo raíz é a base do seu projeto terraform.

No módulo raíz é boa prática deixar as definições de providers e configuraçãos do terraform. Como o exemplo do arquivo **main.tf** desse projeto.

### Módulo filho

Um módulo filho deve estar reunido em um local como por exemplo um diretório que o Módulo raíz consiga acessar.

### A relação de inputs e outputs entre módulos

O módulo raíz transfere **inputs**, informações a serem usadas pelos módulos filho.

Já o módulo filho pode transferir informações de **output** para o módulo raíz. Esse output pode ser apresentado para o operador do terraform (a tela do terminal) ou usado de outra forma.

Nota: essa relação do que é input e output é bom lembrar de linguagens de programação.

### Criação de módulos filho

É boa prática definir módulos filho num arquivo à parte, como **terrafile.tf**.

É usado o bloco `module` para determinar que é do tipo módulo. Os argumentos `source`, `provider` e `version` são parte do bloco `module`, são valores reservados. Geralmente qualquer outra coisa é um argumento de *input*.

Ex:

```
module "web_apps" {
  source = "./web_apps"

  public_key_path = var.public_key_path
  private_key_path = var.private_key_path
  ubuntu_image_name = var.ubuntu_image_name
  hash_commit = var.hash_commit
  blocks = var.blocks
  dynatrace_tenant_URL = var.dynatrace_tenant_URL
  dynatrace_install_token = var.dynatrace_install_token

  providers = {
    aws.oregon = aws.oregon
  }
}
```

### Uso de inputs e outputs

Os *inputs* devem estar definidos no módulo filho. Basicamente são variáveis que aguardam dados vindos do módulo raíz.

Caso o módulo raíz necessitar receber os valores desses inputs por inputs também, arquivos de variáveis ou declarações pelo terminal, deve-se definir no módulo raíz também variáveis.

Ex:

terraform.tfvars -> hash_commit

```
hash_commit = "806d52dafe9b7fddbc4f0d2d41086ed3cfa02a44"
```

variable.Hash_commit

```
variable "hash_commit" { }
```

module.web_apps.variable.has_commit

```
variable "hash_commit" {
  type = string
  default = "806d52dafe9b7fddbc4f0d2d41086ed3cfa02a44"
  description = "The hash commit from IaaSWeek image"
}
```

Já os outputs são enviados via o bloco `output` no módulo filho. E o identificador (nome do output) pode ser usado pelo módulo pai. Como é o caso do exemplo a seguir, que será usado como output também do módulo raíz.

Creio que o `output` pode ser usado para outras coisas além de "um output final para a tela do usuário"

Ex:

module.web_apps.output.ip_address

```
output "ip_address" {
  value = "${aws_instance.web[*].public_ip}"
}
```

output.ip_address

```
output "ip_address" {
  value = module.web_apps.ip_address
}
```

## Backend

Backend é onde fica armazenado o **tfstate**. Ele é opcional, logo, é possivel usar terraform sem a necessidade de cofigurar ele, porém, o **tfstate** será guardado localmente.

### Configuração parcial do backend

É possível fazer definições parciais do backend e passar o resto das informações necessárias como região ou nome do bucket, por exemplo, interativamente, por arquivo ou linha de comando.

* Interativamente: o próprio terraform pede para que você insira as informações faltantes no terminal.
* Arquivo: você deve informar onde o arquivo está localizado com o parâmetro `-backend-config=PATH` quando rodar o `terraform init`. Também é possível usar o Vault, caso existam dados sensíveis no arquivo.
* Linha de comando: ao executar `terraform init` passe o parâmetro `-backend-config="Chave=Valor"`.

No caso do backend **S3** podemos usar variáveis de ambiente. Que é o que estou usando atualmente para passar informações sensíveis

### Diretório *.terraform*

O diretório **.terraform** é criado quando o comando `terraform init` é executado. Nele, são inseridos os plugins dos providers, no caso do exemplo da AWS, e configurações do backend num arquivo **.tfstate**

### Mudança de backends

Quando o backend é retirado, incluido ou modificado o Terraform tem a capacidade de identificar essas mudanças e consegue administrar essas diferentes informações.

Por exemplo, ao retirar o backend do S3 e deixar ele localmente, e continuar a usar o terraform, fazendo *plans* e *applys* terão arquivos distintos de **tfstate** em cada repositório (s3 e local). Caso retorne a usar o S3 antigo, o terraform consegue identificar que existem *tfstates* com diferentes versões e pergunta se quer colocar o atual no novo backend, no caso o S3.

### Comandos *state pull* e *state push*

Os comandos `terraform state pull` e `terraform state push` dão a possibildade de obter e manipular o **tfstate** manualmente e também enviar para o backend.

Esta prática não é aconselhável, porém, se por acaso houver alguma necessidade muito específica, é possível usar estes comandos.

### *State locking*

Uma funcionalidade que pode ser habilitada no backend, se ele der suporte, é o bloqueio de estado.

Esta funcionalidade se torna importante quando há a possibilidade de mais de uma pessoa usar o terraform no ambiente simultaneamente. Alguma mudança simuntânea poderia dar problemas de versão e conflitos nas mudanças.

O locking bloqueia remotamente o acesso ao *state*. Logo, comandos que podem ter uma versão local do *state* continuarão funcionando. Como: `terraform console`, `terraform state`, `terraform taint`.

### State lock com s3 como backend

Para habilitar o bloqueio do estado é preciso ter um dynamoDB configurado para fazer a administração.

Na declaração do bloco de backend é preciso informar qual é a tabela DynamoDB para o *state lock*

Ex:

```
terraform {
  backend "s3" {
    dynamo_table = "dynamo_state_lock"
    bucket = "meu-terraform-bucket"
    key = "estado.tfstate"
    region = "us-east-1"
  }
}
```

OBS: Quando o locking estiver habilitado e por algum acaso você queira desabilitar na execução. É possível usar o parâmetro `-lock=false` no cli do Terraform.

## State

### Por que State?

- Mapear o mundo real: As declarações feitas em código não condizem com o "mundo real". O state fica no meio do caminho entre o código e o que está provisionado para fazer esse mapeamento do que é o que.
- Metadados: O terraform utiliza o *state* para incluir metadados de como a infraestrutura deve comportar, dependências, por exemplo. É um jeito do próprio terraform se localizar no que deve ser feito.
- Performance: O *tfstate* funciona como um mini cache do que há no ambiente, não necessitando consultar o tempo todo o provedor.
  NOTA: Por padrão o terraform sempre dá **refresh** nos recursos ao fazer `plan`. O **refresh** é a consulta no provedor sobre como está o estado atual da infraestrutura. Em pequenos ambientes, não há um custo nisto. Porém, em ambientes grandes, esse tipo de consulta começa a ser oneroso. Para o terraform não fazer **refresh** e se guiar apenas com o *tfstate* é preciso passar o parâmetro `-refresh=false` no cli.
- Sincronia: Ao usar estado remote todos estão "na mesma página" do ambiente.

> DICA DO GOMEX: ferramenta `jq` para a manipulação de arquivos *json*

## Importando recursos

É possível importar recursos legados, que não estão mapeados no terraform, mas que já estão em execução, com o comando `terraform import`.

Para cada tipo de recurso existe a chave requerida para a importação. Por exemplo, em instâncias AWS é necessário passar o ID da instância EC2.

Ex:

```
terraform import module.teste.aws_instance.web[1] i-0edafee0afe568516
```

A importação de recursos complexos, que dependem de outros recursos deve ter maior atenção. Pois o terraform consegue informar a necessidade da escrita de código apenas para o recurso principal, aquele que você ativamente informou o desejo de importar. Logo, os outros recursos adicionais serão importados, porém, quando realizado um novo planejamento, possívelmente serão apagados.

Por exemplo, a importação de **security group**.

Ex do retorno do terminal:

```
/app # terraform import module.teste.aws_security_group.import_sg sg-079ec0393ab8502c6
Error: resource address "module.teste.aws_security_group.import_sg" does not exist in the configuration.

Before importing this resource, please create its configuration in module.teste. For example:

resource "aws_security_group" "import_sg" {
  # (resource arguments)
}

/app # terraform import module.teste.aws_security_group.import_sg sg-079ec0393ab8502c6
Acquiring state lock. This may take a few moments...
module.teste.aws_security_group.import_sg: Importing from ID "sg-079ec0393ab8502c6"...
module.teste.aws_security_group.import_sg: Import prepared!
  Prepared aws_security_group for import
  Prepared aws_security_group_rule for import
  Prepared aws_security_group_rule for import
  Prepared aws_security_group_rule for import
module.teste.aws_security_group_rule.import_sg-2: Refreshing state... [id=sgrule-1259622644]
module.teste.aws_security_group.import_sg: Refreshing state... [id=sg-079ec0393ab8502c6]
module.teste.aws_security_group_rule.import_sg: Refreshing state... [id=sgrule-2732180316]
module.teste.aws_security_group_rule.import_sg-1: Refreshing state... [id=sgrule-1091875501]

Import successful!

The resources that were imported are shown above. These resources are now in
your Terraform state and will henceforth be managed by Terraform.

Releasing state lock. This may take a few moments...
```

Se não ocorrer inclusão no código terraform também dos **aws_security_group_rule** eles serão destruídos no próximo *plan/apply*

Ex do retorno do terminal:

```
  # module.teste.aws_security_group_rule.import_sg will be destroyed
  - resource "aws_security_group_rule" "import_sg" {
      - cidr_blocks       = [
          - "0.0.0.0/0",
        ] -> null
      - from_port         = 22 -> null
      - id                = "sgrule-2732180316" -> null
      - ipv6_cidr_blocks  = [] -> null
      - prefix_list_ids   = [] -> null
      - protocol          = "tcp" -> null
      - security_group_id = "sg-079ec0393ab8502c6" -> null
      - self              = false -> null
      - to_port           = 22 -> null
      - type              = "ingress" -> null
    }

  # module.teste.aws_security_group_rule.import_sg-1 will be destroyed
  - resource "aws_security_group_rule" "import_sg-1" {
      - cidr_blocks       = [
          - "0.0.0.0/0",
        ] -> null
      - from_port         = 25 -> null
      - id                = "sgrule-1091875501" -> null
      - ipv6_cidr_blocks  = [] -> null
      - prefix_list_ids   = [] -> null
      - protocol          = "tcp" -> null
      - security_group_id = "sg-079ec0393ab8502c6" -> null
      - self              = false -> null
      - to_port           = 25 -> null
      - type              = "ingress" -> null
    }

  # module.teste.aws_security_group_rule.import_sg-2 will be destroyed
  - resource "aws_security_group_rule" "import_sg-2" {
      - cidr_blocks       = [
          - "0.0.0.0/0",
        ] -> null
      - from_port         = 0 -> null
      - id                = "sgrule-1259622644" -> null
      - ipv6_cidr_blocks  = [] -> null
      - prefix_list_ids   = [] -> null
      - protocol          = "-1" -> null
      - security_group_id = "sg-079ec0393ab8502c6" -> null
      - self              = false -> null
      - to_port           = 0 -> null
      - type              = "egress" -> null
    }

Plan: 1 to add, 0 to change, 4 to destroy.
```

>DICA: Usar o comando `terraform state show {recurso}` para listar apenas as configurações de estado de um recurso

## **Workspaces**

Workspace é uma funcionalidade dentro do terraform que possibilita utilizar as mesmas configurações do terraform (modulos, backend e código) em diferentes statefiles.

Antes esta funcionalidade tinha o nome de *environments* (ambientes). Este nome aponta bem um dos possíveis usos de workspaces que é o de trabalhar com muitos ambientes, porém com a mesma definição de código, por ambientes é comum a separação em *stage* e *production*.

### Utilizando workspaces

Para criar um novo *workspace* basta usar o comando:

```
terraform workspace new {nome}
```

O terraform automaticamente cria um no*workspace* e já muda para ele.

Para listar os workspaces existentes:

```
terraform workspace list
```

O terraform já tem o workspace chamado de *default*, que é usado caso não sejam criados novos *workspaces*

Para alterar qual *workspece* deseja trabalhar, deve-se utilizar o comando:

```
terraform workspace select {nome-workspace}
```

### *Statefiles* com workspaces

Ao criar e trabalhar com workspaces diferentes do *default* o terraform cria um diretório chamado **env:** e dentro dele cria diretórios com o nome de cada workspace novo. Os statefiles ficam armazenados em seus respectivos diretórios. Logo, há um statefile para cada workspace.

### Criação de recursos na mesma zona

Por utilizar o mesmo código em todos os ambientes, recursos com mesmo nome ou outras caracteristicas podem entrar em conflito variando para cada provedor. Por isso, uma boa recomendação é utilizar diferentes regiões em cada *workspace*.

No caso dos meus testes, houve conflito ao criar dynamodb (com mesmo nome), recursos de redes que dependem de um mesmo vpc com mesmo nome.

### Interpolação de *workspaces*

Para solucionar o problema acima, pode-se utilizar a interpolação de *workspaces* que é a identificação do workspace a ajudar a mudança de configuração de acordo com o workspace.

Como no exemplo abaixo:

```
provider "aws" {
  region = "${terraform.workspace == "prd" ? "us-east-2" : "us-east-1"}"
  version = "~> 2.0"
}
```

## Dados sensíves no *statefile*

É recomendado o uso de backend remoto para manter os dados sensíveis, por ser mais fácil de administrar a segurança deles.

Há uma lista de backends que permitem a criptografia do backend, inclusive o S3 da AWS.

Para habilitar, basta adicionar a opção `encrypt` no S3 de backend. Ele faz criptografia *"server side"*

Configuração de um backend com `encrypt` habilitado:

```
terraform {
  backend "s3" {
    dynamodb_table = "dynamo_state_lock"
    bucket = "meu-terraform-bucket"
    key = "estado.tfstate"
    region = "us-east-1"
    encrypt = true
  }
}
```


-----

## Comandos novos

`terraform state pull`:
> Obtem o estado atual do arquivo **tfstate** do backend. Por padrão o retorno é na console (stdout)

`terraform state push`
> Envia para o backend um novo arquivo, de preferência nova versão, do **tfstate**

`terraform state list`
> Lista tudo o que foi criado segundo o statefile

`terraform state mv`
> Age da mesma forma que o comando `mv` do Linux. Pode ser bastante útil quando se quer redefinir estrutura do projeto sem matar as instâncias criadas, como por exemplo, com a definição de novos módulos para estruturas que já estavam rodando.

`terraform state rm`
> Remove o recurso da gerência do Terraform, excluindo as informações do statefile. É como se o recurso nunca tivesse existido. Ele não é apagado fisicamente do provedor. (Esse comando pode ser útil para a construção de um backend por exemplo)

`terraform import {recurso} {identificador}`
> Importa recursos legados, que não foram criados pelo terraform para dentro do state e consequentemente do terraform.

`terraform refresh`:
> Atualiza o *tfstate* consultando o provedor se houveram mudanças nos recursos

`terraform workspace new`
> Cria um novo workspace

`terraform workspace list`
> Lista os workspaces existentes

`terraform workspace select`
> Muda o ambiente para o workspace selecionado