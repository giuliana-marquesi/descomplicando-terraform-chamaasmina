resource "aws_instance" "web" {
  count                  = var.servers
  ami                    = data.aws_ami.gomex_image.id
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.terraform_resource_kp.key_name
  vpc_security_group_ids = ["${aws_security_group.allow_ssh.id}"]
  subnet_id              = aws_subnet.web_app_teste.id


  provisioner "remote-exec" {
    inline = [
      "touch /tmp/teste"
      #"wget  -O /tmp/Dynatrace-OneAgent-Linux.sh \"${var.dynatrace_tenant_URL}\" --header=\"Authorization: Api-Token ${var.dynatrace_install_token}\"",
      #"sudo /bin/sh /tmp/Dynatrace-OneAgent-Linux.sh --set-app-log-content-access=true --set-infra-only=false",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file(var.private_key_path)
    }
  }

  dynamic "ebs_block_device" {
    for_each = var.blocks
    content {
      device_name = ebs_block_device.value["device_name"]
      volume_size = ebs_block_device.value["volume_size"]
      volume_type = ebs_block_device.value["volume_type"]
    }
  }

  tags = {
    Name        = "HelloWorld"
    Application = "Teste"
  }
}

# west
resource "aws_instance" "west_web" {
  count                  = var.servers_west
  provider               = aws.oregon
  ami                    = data.aws_ami.west_ubuntu.id
  key_name               = aws_key_pair.terraform_resource_kp_west.key_name
  vpc_security_group_ids = ["${aws_security_group.allow_ssh_west.id}", "${aws_security_group.allow_remote_communication.id}"]
  instance_type          = "t2.micro"
  depends_on             = [aws_instance.web]


  provisioner "local-exec" {
    when    = destroy
    command = "echo 'Destroy-me provisioner. (${self.tags.Name})'"
  }

  provisioner "remote-exec" {
    inline = [
      "touch /tmp/teste"
      #"wget  -O /tmp/Dynatrace-OneAgent-Linux.sh \"${var.dynatrace_tenant_URL}\" --header=\"Authorization: Api-Token ${var.dynatrace_install_token}\"",
      #"sudo /bin/sh /tmp/Dynatrace-OneAgent-Linux.sh --set-app-log-content-access=true --set-infra-only=false",
      #"sudo apt-get update",
      #"sudo apt-get -y install default-jdk default-jre maven unzip",
    ]
    connection {
      type        = "ssh"
      user        = "ubuntu"
      host        = self.public_ip
      private_key = file(var.private_key_path)
    }
  }

  tags = {
    Name = "ByeWorld"
  }
}
