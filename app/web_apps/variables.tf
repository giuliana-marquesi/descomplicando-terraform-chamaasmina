variable "hash_commit" {
  type        = string
  default     = "806d52dafe9b7fddbc4f0d2d41086ed3cfa02a44"
  description = "The hash commit from IaaSWeek image"
}

variable "ubuntu_image_name" {
  type        = string
  description = "The image string for searching in AWS"
}

variable "private_key_path" {
  type = string
}

variable "public_key_path" {
  type = string
}

variable "blocks" {
  type = list(object({
    device_name = string
    volume_size = string
    volume_type = string
  }))
  description = "List of EBS block"
}

variable "dynatrace_tenant_URL" {
  type = string
}

variable "dynatrace_install_token" {
  type = string
}

variable "servers_west" {
  type    = number
  default = 1
}

variable "servers" {
  type    = number
  default = 1
}


