output "ip_address" {
  value = "${aws_instance.web[*].public_ip}"
}

output "public_dns" {
  value = "${aws_instance.web[*].public_dns}"
}

output "ip_address_west" {
  value = "${aws_instance.west_web[*].public_ip}"
}

output "public_dns_west" {
  value = "${aws_instance.west_web[*].public_dns}"
}
