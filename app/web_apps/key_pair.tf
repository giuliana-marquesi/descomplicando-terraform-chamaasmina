resource "aws_key_pair" "terraform_resource_kp" {
  key_name   = "terraform_resource_kp"
  public_key = file(var.public_key_path)

  tags = {
    Name = "web_apps"
  }
}

resource "aws_key_pair" "terraform_resource_kp_west" {
  key_name   = "terraform_resource_kp_west"
  public_key = file(var.public_key_path)
  provider   = aws.oregon


  tags = {
    Name = "web_apps"
  }
}
