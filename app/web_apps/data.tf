data "aws_ami" "gomex_image" {
  most_recent = true

  filter {
    name   = "name"
    values = ["IaaSWeek-${var.hash_commit}"]
  }

  owners = ["178520105998"]
}

data "aws_ami" "west_ubuntu" {
  provider    = aws.oregon
  most_recent = true

  owners = ["099720109477"]

  filter {
    name   = "name"
    values = ["${var.ubuntu_image_name}"]
  }
}
