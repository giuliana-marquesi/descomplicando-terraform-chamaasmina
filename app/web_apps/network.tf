# vpc WEB Apps
resource "aws_vpc" "web_apps" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = "true"

  tags = {
    Group = "Web Apps"
  }
}

resource "aws_subnet" "web_app_teste" {

  vpc_id                  = aws_vpc.web_apps.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"

  tags = {
    Group       = "Web Apps"
    Application = "Teste"
  }
}

resource "aws_route_table_association" "web_app_teste_route_web_apps" {
  subnet_id      = aws_subnet.web_app_teste.id
  route_table_id = aws_route_table.route_web_apps.id
}

resource "aws_route_table" "route_web_apps" {
  vpc_id = aws_vpc.web_apps.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.web_apps.id
  }
}

resource "aws_internet_gateway" "web_apps" {
  vpc_id = aws_vpc.web_apps.id

  tags = {
    Group = "Web Apps"
  }
}

# vpc security group
resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.web_apps.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_ssh_west" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = "vpc-f645268e"
  provider    = aws.oregon

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_remote_communication" {
  name     = "dyna_rpc"
  vpc_id   = "vpc-f645268e"
  provider = aws.oregon

  ingress {
    from_port   = 33744
    to_port     = 33744
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
}
