module "teste" {
  source = "./web_apps"

  public_key_path         = var.public_key_path
  private_key_path        = var.private_key_path
  ubuntu_image_name       = var.ubuntu_image_name
  hash_commit             = var.hash_commit
  blocks                  = var.blocks
  dynatrace_tenant_URL    = var.dynatrace_tenant_URL
  dynatrace_install_token = var.dynatrace_install_token
  servers                 = 1
  servers_west            = 1

  providers = {
    aws.oregon = aws.oregon
  }
}
