hash_commit       = "806d52dafe9b7fddbc4f0d2d41086ed3cfa02a44"
ubuntu_image_name = "ubuntu-minimal/images-testing/hvm-ssd/ubuntu-bionic-daily-amd64-minimal*"
private_key_path  = "/app/.ssh/id_rsa"
public_key_path   = "/app/.ssh/id_rsa.pub"
blocks = [
  {
    device_name = "/dev/sdg"
    volume_size = 5
    volume_type = "gp2"
  },
  {
    device_name = "/dev/sdh"
    volume_size = 10
    volume_type = "gp2"
  }
]
