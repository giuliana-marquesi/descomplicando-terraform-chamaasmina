variable "hash_commit" {}

variable "ubuntu_image_name" {}

variable "private_key_path" {}

variable "public_key_path" {}

variable "blocks" {}

variable "dynatrace_tenant_URL" {}
variable "dynatrace_install_token" {}