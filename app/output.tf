output "ip_address" {
  value = module.teste.ip_address
}

output "public_dns" {
  value = module.teste.public_dns
}

output "ip_address_west" {
  value = module.teste.ip_address_west
}

output "public_dns_west" {
  value = module.teste.public_dns_west
}
