provider "aws" {
  region  = "${terraform.workspace == "prd" ? "us-east-1" : "us-east-2"}"
  version = "~> 2.0"
}

provider "aws" {
  alias   = "oregon"
  region  = "${terraform.workspace == "prd" ? "us-west-2" : "us-west-1"}"
  version = "~> 2.0"
}

terraform {
  backend "s3" {
    dynamodb_table = "dynamo_state_lock"
    bucket         = "meu-terraform-bucket"
    key            = "estado.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
}
