# Day 1

## Docker

```
docker container run -it -v $PWD/app:/app meu_terraform:2.0 sh
```

## Principios do Terraform

- infraestrutura imutavel.
	- controi uma imagem que será usada para gerar o recurso
	- cada mudança de infra é codificada, gerada e depois colocada em pra rodar
	- o código é lido pelo binario, que interpreta, compara com estados e depois aplicada a mudança


## Elementos:

- arquivos .tf
- arquivos .tfvars{.json}
- Backend: onde guarda os estados

## Sintaxe HCL:

### Blocos:

```
tipo-dobloco "tipo-do-tipo-do-bloco" "identificador" {
  argumento = "valor"
}

ex: resource "aws_instance" "example" {
  ami = "abc123"

  network_interface {
    # ...
  }
}

```

identificadores são quaisquer campos que servem para identificar. Gpmex disse como exemplo o tipo de bloco e argumento dentro do bloco
Os identificadores não podem ser iniciados com numeros, pode ter _ e -.


## Expressoes:

São conjuntos de informações que podem ser concatenadas e usadas para deixar o codigo terraform mais maleavel

### Valores dos recursos:

podem ser os **Argumentos** que são valores definidos pelo código (por quem escreveu)

E podem ser os **atributos** que são valores só conhecidos depois que o recurso foi criado, retornos do provedor


## Provedores:

### Alias:

é possivel usar mais de um provedor. Para isso existe o recurso de usar **alias**.

quando declarado um novo provider, coloque o meta-argumento **alias** e o valor relativo aquele provedor.

Ex:

```
provider "aws" {
  alias = "west"
  region = "us-west-2"
  version = "~> 2.0"
}
```

com o provedor identificado com alias. é possivel determinar qual provedor usar na declaração da infra-estrutura:

Ex:

```
data "aws_ami" "west_ubuntu" {
  provider = aws.west
  most_recent = true
  owners = ["099720109477"]

  filter {
    name = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"]
  }
}
```

## Variáveis:

O uso das variáveis no Terraform pode ser feito de 5 formas, além da declaração no código `.tf`

### Bloco `variable`:

É preciso declarar as variáveis usando o bloco do tipo `variable`. O bloco pode ter os argumentos:

- `type`: define o tipo da variável, garantindo maior consistência no código e evitando possíveis erros. Como um valor diferente do esperado.

	- o argumento **tipo** pode ser um objeto, contando varios tipos diferentes.

- `default`: é o valor especificado se nenhum outro for passado

- `description`: é a descrição da variável. O uso deste argumento ajuda na manutenção do código, garantindo que ele sirva de documentação.

- `validation` (ainda em fase experimental): é usado para validar se os valor atribuidos à variável são aceitáveis de acordo com as regras pré-estabelecidas. Possui, "sub-argumentos":
	- `condition`: condição. é aqui que é definida a lógica
	- `error_message`: mensagem de erro caso as condições não sejam atendidas. 

Ex:

```
variable "hash_commit" {
  type = string
  default = "806d52dafe9b7fddbc4f0d2d41086ed3cfa02a44"
  description = "The hash commit from IaaSWeek image"
}
```
  

Por ordem de menos "forte" para mais "forte" (quem sobreescreve quem). Temos:

### Variável de ambiente:

As variáveis de ambiente precisam sempre ter o inicio: **TF_VAR_{nomedavariavel}**.

Ex:

```
export TF_VAR_hash_commit="0b20b00524bd34f0f622c8667d3513ba78d4b9c8"
```

### O arquivo `terraform.tfvars`

O arquivo com este nome terá as variáveis declaradas, uma após a outra.
Este arquivo é carregado automaticamente pelo terraform quando executado o comando `plan` sem a necessidade de passar qualquer subcomando *cli*.

Ex:

```
image_id = "ami-abc123"
availability_zone_names = [
  "us-east-1a",
  "us-west-1c",
]
```

### O arquivo `terraform.tfvars.json`

O arquivo com final .json funciona da mesma forma que o **terraform.tfvars**, porém com sintaxe json.

Ex:

```
{
  "image_id": "ami-abc123",
  "availability_zone_names": ["us-west-1a", "us-west-1c"]
}
```

### Arquivos `*.auto.tfvars` ou `*.auto.tfvars.json`

Os arquivos com esta denominação tem o mesmo funcionamento dos arquivos **terraform.tfvars** e **terraform.tfvars.json**.

Acredito que sirvam para separar as variáveis logicamente, podendo reutilizar em partes.

### As opções `-var` e `-var-file` na linha de comando

Quando existem arquivos `.tfvars` sem ter o **auto** ou **terraform** a frente, é preciso passar o caminho com nome do arquivo na linha de comando com o *sub-comando* `-var-file`. Este é o último nível da hierarquia das variáveis, sobreescrevendo todas os outros valores.

Ex:

```
terraform plan -var-file="ubuntu.tfvars" -out plano0
```

Também é possivel passar via linha de comando o valor da variável diretamente, pelo *sub-comando* `-var`.

Ex:

```
terraform plano -var ubuntu_image_name="ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-*"
```

## Provisionamento:

O provisionamento do Terraform não é um recurso recomandado para uso em produção. Ele aparece como última opção. Mesmo assim, o conteúdo sobre seu uso é cobrado na certificação.

### Uso:

O provisionamento pode ser para inserir dados na infraestrutura e também para executar comandos externos de configuração como ansible e blabla.

### Tipos de execução:

- local-exec: execução na máquina que roda o *cli* do Terraform

- remote-exec: execução no recurso criado. Executa atraves de `ssh` ou `winrm`.

### Objeto **self**:

Para obter e manipular informações referentes ao bloco provisionado, faz-se uso do objeto **self**. Com ele é possível obter informações de atributos e argumentos. Diferente de outros momentos do código.

### Quando o provisionamento é executado:

Por padrão é no momento de criação do recurso. Porém, é possivel mudar este comportamento inserindo no código `when = "destroy"`, para executar quando o recurso for destruído e `when = on_failed` quando o recurso falhar.

No caso de provisionar na "destruição", se o recurso estiver marcado como `taint` (falhou no apply, será destruído e recriado no próximo apply) ele não executará.

### Tratamento de falha de provisionamento

Por padrão, caso o provisionamento falhar o recurso falhará. Segundo Gomex: "Terraform quebra".

É possível, no entanto, usar o parametro `on_failure = "continue"` para garantir que o recurso não falhe por conta disso.

### Connection:

Para Provisionamento remoto é recomendado criar um bloco do tipo `Connection`. Ele define como será a conexão remota para o provisionamento. É importante, porque por padrão o terraform deixa uma conexão ssh root sem senha.
_____


## Comandos:

`terraform init` : 
>	ele inicializa a comunicação com os providers, baixando o que é necessário

`terraform plan` :
>	gera um plano que é um arquivo binário

`terraform apply` :
>	pega o plano, cria um estado (que seria uma "foto" de como seria o estado fnal desejado.
>	Depois,compara com estados anteriores e aplica o plano (?) gerando os recurosos necessários.
>	Faz um diff de estados

`terraform destroy`
>	deleta tudo que está provisionado de acordo com o estado

`terraform console` :
>	é console onde é possivel testar e treinar as expressoes
