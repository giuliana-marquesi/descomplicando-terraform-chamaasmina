# day 3

## Dependências

Quando para existir um recurso ele necessita da existência de outro, seja por conta da exigencia de argumentos específicos, ou  da própria arquitetura da infraestrutura, isto é uma dependência.

O Terraform consegue lidar automáticamente com dependências, caso elas estejam implicitas no código. Esta inteligência com as dependência é muito importante para determinar quando um recurso deve ser criado e destruído para não haver conflito.

Existem dois tipos de dependência no Terraform.

### Dependência implícita

A dependência implícita, como o nome já diz, não está declarada no código. Não há um argumento no recurso falando: "Este recurso depende daquele".

O Terraform identifica a dependência implícita pelas interpolações (quando você aponta atributos de outro recurso do código).

Como por exemplo, a relação de *"bucket_objects"* com um *"bucket"*. É necessário informar um *bucket* na construção de um recurso *bucket_object*

```
resource "aws_s3_bucket_object" "testszip" {
  bucket = aws_s3_bucket.bucket.bucket
  provider = aws.ohio
  key = "tests.zip"
  source = var.dt_bucket_object_path["testszip"]
}
```

Neste caso, o "nome" do bucket é obtido de outro recurso que será criado, o bucket com "nome" de bucket no Terraform. Por existir essa relação entre recursos pela interpolação (`aws_s3_bucket.bucket.bucket`), o Terraform primeiro irá criar completamente o S3 para então criar o *bucket_object*.

PS: Antes estava passando uma variável no argumento **`bucket`** do objeto acima, com isso, o terraform tentava criar simultaneamente o *bucket* e o *bucket_object* e falhava.

### Dependência explícita

A dependência explícita é quando você informa via código que um recurso depende de outro. Um uso para este tipo é quando não há um argumento que crie essa dependência.

Para informar que um recurso depende de outro, basta incluir o argumento: `depends_on = [{recurso}]`

```
resource "aws_instance" "west_web" {
  count = var.servers_west
  provider = aws.oregon
  ami = data.aws_ami.west_ubuntu.id
  key_name = aws_key_pair.terraform_resource_kp_west.key_name
  vpc_security_group_ids = ["${aws_security_group.allow_ssh_west.id}", "${aws_security_group.allow_remote_communication.id}"]
  instance_type = "t2.micro"
  depends_on = [ aws_instance.web]

  ...
}
```

No exemplo acima, a intância **"west_web"** só será criada quando a instância **"web"** estiver pronta. A **"west_web"** depende da **"web"**

### Criação de recursos em paralelo

O terraform tenta criar recursos em paralelo para reduzir o tempo total na aplicação de mudanças. Porém, isso depende da trama de dependências que devem ser respeitadas.

## *Debbuging Terraform*

Saber ativar o modo *debug* é importante para entender o funcionamento "por trás" do Terraform quanto para resolver problemas.

Para ativar o log basta incluir o parâmetro `TF_LOG` junto com o comando cli que deseja. O `TF_LOG` possui diferentes níveis de "verbosidade". Os níveis são:

* `TRACE`
* `DEBUG`
* `INFO`
* `WARN`
* `ERROR`

Exemplo de uso do LOG:

```
TF_LOG=TRACE terraform apply plano0
```

### Arquivos de log

É possível configurar para que o terraform salve o debbug em arquivos quando o parametro de LOG é usado.

Para determinar a saída do debug para um arquivo basta inserir na linha de comando o parâmetro: `TF_LOG_PATH={caminho-do-arquivo}`. O parâmetro `TF_LOG` também deve estar incluso.

Ex:

```
TF_LOG_PATH=info.log TF_LOG=INFO terraform plan -out plano0
```

## Comando *TAINT*

O comando `taint` marca um recurso como *"tainted"* manualmente. Um recurso marcado desta forma irá ser destruído e recriado no próximo ciclo de *plan-apply*.

Ele é usado quando existem mudanças no recurso que o Terraform "não identifica como passivel de mudança" como mudanças de provisionamento.

Dependências de recursos marcados para `taint` também são afetados.

O parâmetro `-allow-missing` pode ser bastante útil no uso de CI/CD. Pois em caso do recurso não existir ele finalizar sem falhas. Do contrário, caso o recurso marcado não exista ele retorna erro.

Exemplo de uso do comando:

```
terraform taint module.teste.aws_vpc.web_apps
```

Caso o recurso tenha sido criado por for_each

```
terraform taint 'module.route_tables.azurerm_route_table.rt[\"DefaultSubnet\"]'
```

## Comando *GRAPH*

O terraform usa a estrutura de grafos para controlar a execução de seus comandos. A estrutura de grafos orienta o terraform sobre quais são os recursos (entra aqui providers, data e outras coisas) dependem do que e como eles se relacionam.

Para cada comando é gerado um novo grafo.

Além dos grafos serem usados internamente pelo Terraform, há o comando `graph` que exporta este grafo num arquivo no formato **DOT**. 

Por padrão o grafo retornado é do comando **plan**, porém, é possível escolher qual o "tipo" de grafo com o subcomando `-type={tipo}`. Os tipos podem ser: `plan`, `plan-destroy`, `apply`, `validate`, `input`, `refresh`.

Com o programa **GraphViz** é possível converter o grafo exportado para o formato .svg.

Para instalar o **GraphViz** no container:

```
apk -U add graphviz
```

Exemplo de uso com GraphViz:

```
terraform graph -type=plan | dot -Tsvg > graph.svg
```

Seguem os grafos de cada tipo que executei até então:

* apply:
![apply](app/grafos/apply.svg)

* plan-destroy:
![plan-destroy](app/grafos/plan-destroy.svg)

* plan:
![plan](app/grafos/plan.svg)

* refresh:
![refresh](app/grafos/refresh.svg)

* validate:
![validate](app/grafos/validate.svg)

OBS: Depois preciso entender um pouco mais a diferença entre eles e quando é que ocorre a definição das informações exportadas (em qual momento gera cada tipo de grafo)


## Comando *FMT*

Este comando é um formatador de texto que coloca o código terraform seguindo os padrões recomendados.

Uma coisa linda demais!

Em pipelines é bom usar com outros subcomandos como `-check`, que retorna valor diferente de 0 quando há alguma alteração a ser feita, e o `-diff`, que compara o arquivo antes e depois de modificado.

Lembrando que o `fmt` por padrão já modifica e salva seus arquivos automaticamente. Para isto não acontecer, precisa passa o subcomando `-write=false` ou `-check`.

## Comando *VALIDATE*

O comando `validate` valida o código localmente. Se ele atende o que é necessário para funcionar bem.

O `validate` é muito útil na *pipeline* como ponto inicial, pois ele já tras um retorno rapido do código.

-----

## Mais comandos:

`terraform taint`
> Marca um recurso como *tainted*, ou seja, no próximo ciclo irá ser destruído e recriado

`terraform untaint`
> Desmarca um recurso como *tainted*

`terraform graph`
> Exporta o grafo com as relações entre recursos de seu projeto terraform

`terraform fmt`
> Formata seus arquivos Terraform para o padrão estabelecido

`terraform validate`
> Valida o código localmente para identificar se há alguma configuração errada